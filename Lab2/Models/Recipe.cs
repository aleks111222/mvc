using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Lab2.Models
{
    public class Recipe
    {
        public String Name { get; set; }

        public String[] Description { get; set; }

        public List<Ingredient> Ingredients { get; set; }
    }

    public class Ingredient
    {
        public String Name { get; set; }

        public String Quantity { get; set; }

        public String Unit { get; set; }
    }
}
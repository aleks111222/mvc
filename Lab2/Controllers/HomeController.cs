﻿using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Lab2.Models;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.IO;
using System;

namespace Lab2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {

            _logger = logger;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public ActionResult Index()
        {
            getData();
            return View();
        }

        public List<Recipe> GetRecipes()
        {
            List<Recipe> recipes = new List<Recipe>();

            if (!System.IO.File.Exists("recipes.txt"))
            {
                System.IO.File.WriteAllText("recipes.txt", "{}");
            }

            string fileContent = System.IO.File.ReadAllText("recipes.txt", Encoding.UTF8);
            var jsonData = JObject.Parse(fileContent);

            foreach(var jElement in jsonData)
            {   
                List<string> descriptionList = new List<string>();

                foreach (string descriptionSentence in jsonData[jElement.Key]["recipe"])
                {
                    descriptionList.Add(descriptionSentence + "\n");
                }

                var recipe = new Recipe()
                {
                    Name = jElement.Key,
                    Description = descriptionList.ToArray()
                };

                recipe.Ingredients = new List<Ingredient>();
                JObject jIngredients = (JObject)jsonData[jElement.Key];
                recipe.Ingredients = new List<Ingredient>();

                foreach(var jIngredient in jIngredients)
                {
                    if (jIngredient.Key == "recipe") continue;

                    Ingredient ingredient = new Ingredient() // nie dziala dla takich samych name---*******************************
                    {
                        Name = (string)jIngredient.Key.Trim().Replace(" (different measure)", "") + " ",
                        Quantity = Regex.Match((string)jIngredient.Value, @"[-+]?[0-9]*\.?[0-9]+").Groups[0].Value + " ",
                        Unit = ((string)jIngredient.Value).Split(Regex.Match((string)jIngredient.Value, @"[-+]?[0-9]*\.?[0-9]+").Groups[0].Value)[1].Trim() + "\n"
                    };
                    
                    recipe.Ingredients.Add(ingredient);
                }

                recipes.Add(recipe);
            }

            return(recipes);
        }
        public ActionResult getData()
        {
            List<Recipe> recipes = GetRecipes();
            return View(recipes);
        }
        public ActionResult Edit(string RecipeName)
        {
            List<Recipe> recipes = GetRecipes();
            Recipe recipeEdit = new Recipe();
            foreach(Recipe recipe in recipes)
            {
                if(recipe.Name == RecipeName)
                {
                    recipeEdit = recipe;
                    break;
                }
            }
            return View("Add", recipeEdit);
        }

        public ActionResult Add()
        {
            Recipe recipe = new Recipe()
            {
                Name = "",
                Description = new string[] {""},
                Ingredients = new List<Ingredient>()
            };
            return View("Add", recipe);
        }
        
        public ActionResult cancelRecipe()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult okRecipe(string RecipeName, string Description, string Ingredients, string OldKey)
        {
            applyRecipe(RecipeName, Description, Ingredients, OldKey);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult applyRecipe(string RecipeName, string Description, string Ingredients, string OldKey)
        {
            RecipeName = RecipeName.Trim();
            string[] DescriptionArray = Regex.Split(Description.Trim(), "\r\n|\r|\n");
            string[] IngredientsArray = Regex.Split(Ingredients.Trim(), "\r\n|\r|\n");

            JObject newElement =
            new JObject
            (
                new JProperty
                (RecipeName,
                    new JObject
                    (
                        new JProperty
                        ("recipe",
                            new JArray
                            (DescriptionArray
                            )
                        )
                    )
                )
            );
            string a = @"[\s]*([A-Za-z0-9()]+[\s]*)+[+]?([0-9]*[.])?([0-9])+([\s]+)([A-Za-z0-9]+[\s]*)+";

            Regex regex = new Regex(a);

            string fileContent = System.IO.File.ReadAllText("recipes.txt", Encoding.UTF8);
            var jsonData = JObject.Parse(fileContent);

            Recipe emergencyRecipe = new Recipe()
                {
                    Name = RecipeName,
                    Description = Description.Split("\n"),
                    Ingredients = new List<Ingredient>()
                };

            foreach(Recipe recipe in GetRecipes())
                {
                    if(recipe.Name == OldKey)
                    emergencyRecipe = recipe;
                }
            
            JObject jObj = (JObject)JsonConvert.DeserializeObject(fileContent);
            //int count = jObj[RecipeName].Count();

            foreach(string IngredientEntry in IngredientsArray)
            {
                if(IngredientEntry == "") continue;

                MatchCollection matchCollection = regex.Matches(IngredientEntry);
                if (matchCollection.Count == 0)
                {
                    return View("Add", emergencyRecipe);
                }

                string Quantity = Regex.Match(IngredientEntry, @"[-+]?[0-9]*\.?[0-9]+").Groups[0].Value + " ";
                string Name = IngredientEntry.Split(Quantity)[0].Trim();
                string Unit = IngredientEntry.Split(Quantity)[1].Trim();

                JObject jRecipeName = (JObject)newElement[RecipeName];
                restart:
                foreach (var oldIngredient in jRecipeName)
                {
                    if(oldIngredient.Key == "recipe") continue;

                    string oldName = (string)oldIngredient.Key.Trim();
                    string oldQuantity = Regex.Match((string)oldIngredient.Value, @"[-+]?[0-9]*\.?[0-9]+").Groups[0].Value.Trim();
                    string oldUnit = ((string)oldIngredient.Value).Split(' ')[1].Trim();

                    if(oldName == Name)
                    {
                        if(oldUnit == Unit)
                        {
                            return View("Add", emergencyRecipe);
                        }
                        else
                        {
                            Name += " (different measure)";
                            goto restart;
                        }
                    }
                }

                JProperty jNewIngredient = new JProperty(Name, Quantity + Unit);

                jRecipeName.Property("recipe").AddAfterSelf(jNewIngredient);
            }

            if(OldKey != null && OldKey != "" && OldKey != RecipeName)
            {
                jsonData.Property(OldKey).Remove();
                System.IO.File.WriteAllText("recipes.txt", jsonData.ToString());
            }

            foreach(var jElement in jsonData)
            {
                if(RecipeName == jElement.Key)
                {
                    jsonData.Property(RecipeName).Remove();
                    System.IO.File.WriteAllText("recipes.txt", jsonData.ToString());
                    break;
                }
            } 
            fileContent = System.IO.File.ReadAllText("recipes.txt", Encoding.UTF8);

            using (FileStream fileStream = System.IO.File.OpenWrite("recipes.txt"))
            {
                StreamWriter writer = new StreamWriter(fileStream);
                if (fileContent.Length > 2) 
                {
                    fileStream.Seek(fileContent.Length - 3, SeekOrigin.Begin);
                    writer.Write(",");
                }
                else fileStream.Seek(fileContent.Length - 1, SeekOrigin.Begin);
                
                writer.Write(newElement.ToString().Substring(1, newElement.ToString().Length - 1));
                writer.Flush();
                fileStream.Close();
            }

            foreach(Recipe recipe in GetRecipes())
                {
                    if(recipe.Name == RecipeName)
                    emergencyRecipe = recipe;
                }

            return View("Add", emergencyRecipe);
        }

        public IActionResult Delete(string RecipeName)
        {
            string fileContent = System.IO.File.ReadAllText("recipes.txt", Encoding.UTF8);
            var jsonData = JObject.Parse(fileContent);

            jsonData.Property(RecipeName).Remove();
            System.IO.File.WriteAllText("recipes.txt", jsonData.ToString());


            return RedirectToAction("Index");
        }

        public IActionResult ShoppingList()
        {
            List<Recipe> recipes = GetRecipes();
            List<Ingredient> ingredients = new List<Ingredient>();
            string EnterRecipes = "";
            return View("ShoppingList",Tuple.Create(recipes, ingredients, EnterRecipes));
        }

        [HttpPost]
        public IActionResult CalculateIngredients(string EnterRecipes)
        {
            List<Recipe> recipes = GetRecipes();
            List<Ingredient> ingredients = new List<Ingredient>();
            bool duplicate = false;

            foreach(string iRecipe in EnterRecipes.Trim().Split("\n"))
            {
                foreach(Recipe jRecipe in recipes)
                {
                    if(jRecipe.Name == iRecipe.Trim())
                    {
                        if(ingredients.Count == 0)
                        {
                            ingredients.AddRange(jRecipe.Ingredients);
                            continue;
                        }
                        foreach(Ingredient ingredient2 in jRecipe.Ingredients)
                        {
                            duplicate = false;
                            foreach(Ingredient ingredient1 in ingredients)
                            {
                                if(ingredient2.Name.Replace("(different measure)", " ") == ingredient1.Name.Replace("(different measure)", " ") && ingredient2.Unit.Trim() == ingredient1.Unit.Trim())
                                {
                                    float q = float.Parse(ingredient1.Quantity) + float.Parse(ingredient2.Quantity);
                                    ingredient1.Quantity = q.ToString() + " ";
                                    duplicate = true;
                                    break;
                                }
                            }
                            if(!duplicate)
                            {
                                ingredients.Add(ingredient2);
                            }

                        }
                    }
                }
            }

            return View("ShoppingList",Tuple.Create(recipes, ingredients, EnterRecipes));
        } 
    }
}

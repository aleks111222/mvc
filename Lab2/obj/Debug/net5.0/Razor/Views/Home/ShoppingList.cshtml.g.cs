#pragma checksum "C:\Users\Lenovo\Desktop\Lab2\Views\Home\ShoppingList.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8198c2f037be08a6acbc2ffde9095af14e98afb9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_ShoppingList), @"mvc.1.0.view", @"/Views/Home/ShoppingList.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Lenovo\Desktop\Lab2\Views\_ViewImports.cshtml"
using Lab2;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Lenovo\Desktop\Lab2\Views\_ViewImports.cshtml"
using Lab2.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8198c2f037be08a6acbc2ffde9095af14e98afb9", @"/Views/Home/ShoppingList.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fd9d6898af3ee645766c02769f00bd7edd74cd52", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_ShoppingList : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Tuple<List<Lab2.Models.Recipe>, List<Ingredient>, string>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-group"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\Lenovo\Desktop\Lab2\Views\Home\ShoppingList.cshtml"
  
    ViewData["Title"] = "Recipes";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<html>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8198c2f037be08a6acbc2ffde9095af14e98afb93889", async() => {
                WriteLiteral("\r\n\r\n");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8198c2f037be08a6acbc2ffde9095af14e98afb94151", async() => {
                    WriteLiteral(@"
        <div class=""d-flex justify-content-between align-items-center"">
            <label for=""EnterRecipes"">Enter Recipes</label>
            <input type=""submit"" class=""btn btn-primary my-1"" name=""Calculate"" value=""Calculate""
            formaction=""CalculateIngredients"" formmethod=""post""/>
        </div>
            <textarea class=""form-control"" id=""EnterRecipes"" name=""EnterRecipes"" required=""required"" rows=""4"" cols=""50"">");
#nullable restore
#line 15 "C:\Users\Lenovo\Desktop\Lab2\Views\Home\ShoppingList.cshtml"
                                                                                                                   Write(Model.Item3);

#line default
#line hidden
#nullable disable
                    WriteLiteral("</textarea>\r\n        <br>\r\n            <label for=\"ShoppingList\">Shopping List</label>\r\n        <br>\r\n            <textarea disabled class=\"form-control\" id=\"ShoppingList\" name=\"ShoppingList\" rows=\"4\" cols=\"50\">");
#nullable restore
#line 19 "C:\Users\Lenovo\Desktop\Lab2\Views\Home\ShoppingList.cshtml"
                                                                                                              foreach (var ingredient in Model.Item2)
            {
                

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "C:\Users\Lenovo\Desktop\Lab2\Views\Home\ShoppingList.cshtml"
            Write(ingredient.Name + ingredient.Quantity + ingredient.Unit);

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "C:\Users\Lenovo\Desktop\Lab2\Views\Home\ShoppingList.cshtml"
                                                                          ;
            }

#line default
#line hidden
#nullable disable
                    WriteLiteral("</textarea>\r\n        <br>\r\n            <label for=\"KnownRecipes\">Your Recipe List</label>\r\n        <br>\r\n            <textarea disabled class=\"form-control\" id=\"KnownRecipes\" name=\"KnownRecipes\" rows=\"4\" cols=\"50\">");
#nullable restore
#line 26 "C:\Users\Lenovo\Desktop\Lab2\Views\Home\ShoppingList.cshtml"
                                                                                                              foreach (var Recipe in Model.Item1)
            {
                

#line default
#line hidden
#nullable disable
#nullable restore
#line 28 "C:\Users\Lenovo\Desktop\Lab2\Views\Home\ShoppingList.cshtml"
            Write(Recipe.Name + "\n");

#line default
#line hidden
#nullable disable
#nullable restore
#line 28 "C:\Users\Lenovo\Desktop\Lab2\Views\Home\ShoppingList.cshtml"
                                     ;
            }

#line default
#line hidden
#nullable disable
                    WriteLiteral("</textarea>\r\n");
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</html>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Tuple<List<Lab2.Models.Recipe>, List<Ingredient>, string>> Html { get; private set; }
    }
}
#pragma warning restore 1591
